from card import Card
from config import Config
from generateHtml import generate_html
from get_song_ids_from_tracklist import get_song_ids_from_tracklist
from page import Page


def main():
    # Spotify API Client ID
    client_id = input("Please enter your Spotify API Client ID:")
    # Spotify Secret ID
    client_secret = input("Please enter your Spotify API Client ID:")
    config = Config(client_id, client_secret)
    # Request the trakclistids
    track_ids = input("Please enter tracklist IDs separated by commas(,)")
    track_ids = track_ids.strip().split(',')
    #track_ids = ["0gxI7QhXZy4ayY9pcYbVRh" = Saját lista, "37i9dQZF1DX0XUsuxWHRQd" = RapCaviar,"1puQ0hv40TUre24cFillJS" = Best Rock songs of All Time]

    # Get songs
    for track_id in track_ids:
        song_ids, song_list_name = get_song_ids_from_tracklist(track_id, config.client_id, config.client_secret)
        pages = []
        page = Page()
        # Generate pages. Every page must content 12 card, becouse if less then 12 the printing will over slide
        for c in song_ids:
            if page.count_cards() == 12:
                pages.append(page)
                page = Page()
            card = Card(c, config, song_list_name)
            page.add_card(card)
        if page.count_cards() == 12:
            pages.append(page)

        generate_html(pages, song_list_name)


if __name__ == "__main__":
    main()