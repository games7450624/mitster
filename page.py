class Page:
    def __init__(self):
        self.cards = []

    def add_card(self, card):
        if len(self.cards) < 12:
            self.cards.append(card)
        else:
            raise ValueError("The page is full")

    def count_cards(self):
        return len(self.cards)

    def generate_card_html(self):
        card_html = ''
        for card in self.cards:
            card_html += '<div class="box">\n'
            card_html += f'<p class="artist">{card.artist}</p>\n'
            card_html += f'<p class="year">{card.year}</p>\n'
            card_html += f'<p class="song">{card.song}</p>\n'
            card_html += f'<p class="nr">{card.nr}</p>\n'
            card_html += '</div>\n'
        return card_html

    def generate_qr_card_html(self):
        desired_order = [2, 1, 0, 5, 4, 3, 8, 7, 6, 11, 10, 9]
        qr_card_html = ''
        for i in desired_order:
            card = self.cards[i]
            qr_card_html += '<div class="boxqr">\n'
            qr_card_html += f'<img src="./img/{card.qr_code}">\n'
            qr_card_html += f'<p class="nr">{card.nr}</p>\n'
            qr_card_html += '</div>\n'
        return qr_card_html
