import common
def generate_html(pages, song_list_name):
    song_list_name = common.name_normaize(song_list_name)
    with open("html/" + song_list_name + "/" + song_list_name + ".html", "w", encoding="utf-8") as hitsterhtml:
        hitsterhtml.write("""<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>"""+song_list_name+"""</title>
  <style>
    *{
      margin: 0;
    }
    .boxs {
      display: flex;
      flex-wrap: wrap;
      justify-content: center;
      width: 900px;
      margin: auto;
    }
    .box, .boxqr {
      height: 270px;
      width: 270px;
      padding: 5px;
      margin: 5px;
      border: 1px solid black;
    }
    .box{
      background-color: #b4d8dc;
      position: relative;

    }

    .boxqr{
      background-image: url('img/background.png');
      background-size: cover; 
      background-position: center;
      background-repeat: no-repeat;
    }

    p, i{
      font-family: Arial, sans-serif;
      color: black;
    }

    .box i, .box p {
      position: absolute;
      width: 96.2%;
    }

    .artist {
      font-size: 23px;
      top: 27px;
      text-align: center;
    }
    .year {
      font-family: monospace;
      font-size: 90px;
      top: 90px;
      font-weight: 800;
      text-align: center;
    }

    .song {
      font-style: italic;
      font-size: 20px;
      text-align: center;
      top: 225px;
      overflow: hidden;
      max-height: 48px;
    }
    .nr {
      color: white;
      font-size: 10px;
      text-align: right;
      top: 260px;
      right: 10px;
   }

   .boxqr .nr{
     margin: -15px 0px;
   }

    .boxqr img{
      width: 114px;
      margin: 78px;
    }

  </style>
</head>
<body>
<div class="boxs">""")
        # Dynamic html
        for page in pages:
            hitsterhtml.write(page.generate_card_html())
            hitsterhtml.write(page.generate_qr_card_html())
        hitsterhtml.write("""</div>
</body>
</html>""")


