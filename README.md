Desc:

	Hitster card form spotify playlist.
	The code generat a html doc which name will be the tracklist_name in a tracklistname dictionary.


How use it:
	You need a spotify developers account:

	https://developer.spotify.com/documentation/web-api/concepts/apps

	- Where you must create an app. If you make a random app you will get an spotify Client ID and Client secret.
	- If you want use this program the Client ID and Client secret is neccesary.
	- After that, let's browsing in the spotify and find a tracklist.
	- In the browser the track list is looking like this:

		https://open.spotify.com/playlist/6i2Qd6OpeRBAzxfscNXeWp

	- When the program asks the TrackListIds, have to gives this id: 6i2Qd6OpeRBAzxfscNXeWp
	- This string is end of the link
	
Requirements:

	Python 3.12. on desktop
	Spotify app on phone
	qr scanner on phone

1, Virtual env:

	python -m venv venv
	venv\Scripts\activate  # Windows
	pip install -r requirements.txt

2, Run:

	python mitser.py


Print setting:

	I tested the printing in Chrome, and I must setted the margins. 
	LaYout: Portrait
	Margins: Custom
		MarginTop: 10mm
		MarginLeft: 4mm
		MarginRight: 4mm
		MarginBottom: 24mm