import spotipy
from spotipy.oauth2 import SpotifyClientCredentials


def get_song_ids_from_tracklist(playlist_id, client_id, client_secret):
    try:
        # Spotify API authentication
        client_credentials_manager = SpotifyClientCredentials(client_id=client_id, client_secret=client_secret)
        sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)

        # Request tracklist datas
        playlist = sp.playlist(playlist_id)
        song_list_name = playlist['name']
        total_tracks = playlist['tracks']['total']

        # Spotify 100 limit hack
        track_ids = []
        limit = 100
        offset = 0

        while offset < total_tracks:
            # Request tracklist songs
            playlist_tracks = sp.playlist_tracks(playlist_id, limit=limit, offset=offset)
            # Request songs datas
            track_ids.extend([track['track']['id'] for track in playlist_tracks['items'] if track['track'] is not None])
            offset += limit
        print(len(track_ids))
        return track_ids, song_list_name

    except spotipy.SpotifyException as e:
        print("Other exception(Spotify):", e)
        return []
