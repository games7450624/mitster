class Config:
    def __init__(self, client_id, secret):
        self._client_id = client_id
        self._client_secret = secret

    @property
    def client_id(self):
        return self._client_id

    @property
    def client_secret(self):
        return self._client_secret
