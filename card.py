import shutil

import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import qrcode
import common
import os

class Card:
    id_counter = 1

    def __init__(self, id, config, song_list_name):
        self._id = id
        self._artist = None
        self._year = None
        self._song = None
        self._qr_code = None
        self._nr = Card.id_counter
        self._song_list_name = song_list_name
        Card.id_counter += 1
        self.set_card_data(config)
        self.generate_QRcode()

    # Getter methods
    @property
    def id(self):
        return self._id

    @property
    def artist(self):
        return self._artist

    @property
    def year(self):
        return self._year

    @property
    def song(self):
        return self._song

    @property
    def nr(self):
        return self._nr

    @property
    def qr_code(self):
        return self._qr_code

    def set_card_data(self, config):
        try:
            client_credentials_manager = SpotifyClientCredentials(
                client_id=config.client_id, client_secret=config.client_secret)
            sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)

            track_id = self._id

            track_info = sp.track(track_id)

            print(track_info['artists'][0]['name'])
            self._artist = track_info['artists'][0]['name']
            print(track_info['name'])
            self._song = track_info['name']
            print(track_info['album']['release_date'][:4])
            self._year = track_info['album']['release_date'][:4]
            self.generate_QRcode()

        except spotipy.SpotifyException as e:
            print("Error while calling SpotiFy API:", e)
        except KeyError as e:
            print("Spotify response not include the data", e)
        except Exception as e:
            print("Other exception(Spotify)", e)

    def generate_QRcode(self):
        try:
            link = "https://open.spotify.com/track/" + self._id

            # QR code params
            qr = qrcode.QRCode(
                version=1,
                error_correction=qrcode.constants.ERROR_CORRECT_L,
                box_size=10,
                border=0,
            )

            qr.add_data(link)
            # QR code create
            qr.make(fit=True)

            # QR code generaten into image
            img = qr.make_image(fill='black', back_color='white')

            # Pic sava
            normalized_track_list_name = common.name_normaize(self._song_list_name)
            file_name = str(self._nr) + ".png"
            dir_path = "html/" + normalized_track_list_name + "/img/"
            # Dictionary handling
            if not os.path.exists(dir_path):
                os.makedirs(dir_path)

            shutil.copy('background.png', os.path.join(dir_path, 'background.png'))

            img.save("html/" + normalized_track_list_name + "/img/" + file_name)
            self._qr_code = file_name
        except Exception as e:
            print("Hiba történt a QR kód generálása során:", e)
