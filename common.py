import unicodedata
import re

def name_normaize(song_list_name):
    #Generate file name from a string
    song_list_name = unicodedata.normalize('NFKD', song_list_name).encode('ascii', 'ignore').decode('ascii')
    song_list_name = song_list_name.lower()
    song_list_name = re.sub(r'[^a-z0-9]+', '_', song_list_name)
    song_list_name = song_list_name.strip('_')
    return song_list_name